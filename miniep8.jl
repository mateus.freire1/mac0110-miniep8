using Test

function troca(v, i, j)
   aux = v[i]
   v[i] = v[j]
   v[j] = aux
end

function insercao(v)
  tam = length(v)
  for i in 2:tam
    j = i
    while j > 1
        if compareByValueAndSuit(v[j],v[j - 1])
        troca(v, j, j - 1)
      else
        break
      end
     j = j - 1
    end
  end
 return v
end

function compareByValue(x,y)
  cartas = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
  valorx = x[1:end-1]
  valory = y[1:end-1]
  for i in 1:length(cartas)
     if valorx == cartas[i]
        valorx = i
     break
     end     
  end  
  for i in 1:length(cartas)
     if valory == cartas[i]
        valory = i
     break
     end     
  end            
   if valorx < valory
      return true
   else
      return false
   end
end

function compareByValueAndSuit(x,y)
  naipes = ["♦", "♠", "♥", "♣"]
  cartas = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
  valorx = x[1:end-1]
   naipex = x[end:end]
  valory = y[1:end-1]
   naipey = y[end:end]
  for i in 1:length(cartas)
     if valorx == cartas[i]
        valorx = i
     break
     end     
  end  
     for i in 1:length(naipes)
       if naipex == naipes[i]
          naipex = i
         break
       end     
     end    
  for i in 1:length(cartas)
     if valory == cartas[i]
        valory = i
     break
     end     
  end 
     for i in 1:length(naipes)
       if naipey == naipes[i]
          naipey = i
         break
       end     
     end       
   if naipex < naipey
      return true
   elseif naipex == naipey     
      if valorx < valory
       return true
      else
         return false
      end
   else
       return false
   end
end

function test()
   if !compareByValue("2♠" ,"A♠") ||
      !compareByValue("10♣" ,"K♦") ||
      compareByValue("Q♠" ,"10♠") ||
      compareByValue("A♦" ,"J♦")
     return "Erro na função CompareByValue"
   end
   if !compareByValueAndSuit("K♥" ,"1♣") ||
      !compareByValueAndSuit("10♦" ,"10♠") ||
      compareByValueAndSuit("2♥" ,"A♠") ||
      compareByValueAndSuit("K♠" ,"10♠")   
     return "Erro na função CompareByValueAndSuit"
   end
     @test insercao(["10♥","10♦","K♠","A♠","J♠","A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
	 @test insercao(["A♥","A♦","1♣","A♠","J♠","9♥"]) == ["A♦", "J♠", "A♠", "9♥", "A♥", "1♣"]
	 @test insercao(["A♦","Q♠","1♥","10♠","J♣","K♦"]) ==  ["K♦", "A♦", "10♠", "Q♠", "1♥", "J♣"] 
  return "Final dos Testes"
end

println(test())
